def encrypt_vigenere(plaintext, keyword):
    alph = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
    ALPH = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ciphertext = ''
    if len(plaintext) > len(keyword):
        keyword *= (len(plaintext) // len(keyword) + 1)
    for i in range(len(plaintext)):
        if plaintext[i] in alph:
            if keyword[i] in alph:
                find1 = alph.find(plaintext[i])
                find2 = alph.find(keyword[i])
                ciphertext += alph[find1 + find2]
            else:
                find1 = alph.find(plaintext[i])
                find2 = ALPH.find(keyword[i])
                ciphertext += alph[find1 + find2]
        elif plaintext[i] in ALPH:
            if keyword[i] in alph:
                find1 = ALPH.find(plaintext[i])
                find2 = alph.find(keyword[i])
                ciphertext += ALPH[find1 + find2]
            else:
                find1 = ALPH.find(plaintext[i])
                find2 = ALPH.find(keyword[i])
                ciphertext += ALPH[find1 + find2]
        else:
            ciphertext += plaintext[i]
    return ciphertext


def decrypt_vigenere(ciphertext, keyword):
    alph = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
    ALPH = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'
    plaintext = ''
    if len(ciphertext) > len(keyword):
        keyword *= (len(ciphertext) // len(keyword) + 1)
    for i in range(len(ciphertext)):
        if ciphertext[i] in alph:
            if keyword[i] in alph:
                find1 = alph.find(ciphertext[i])
                find2 = alph.find(keyword[i])
                plaintext += alph[find1 - find2]
            else:
                find1 = alph.find(ciphertext[i])
                find2 = ALPH.find(keyword[i])
                plaintext += alph[find1 - find2]
        elif ciphertext[i] in ALPH:
            if keyword[i] in alph:
                find1 = ALPH.find(ciphertext[i])
                find2 = alph.find(keyword[i])
                plaintext += ALPH[find1 - find2]
            else:
                find1 = ALPH.find(ciphertext[i])
                find2 = ALPH.find(keyword[i])
                plaintext += ALPH[find1 - find2]
        else:
            plaintext += ciphertext[i]
    return(plaintext)
