import webbrowser
import argparse
import requests
import time
import plotly
import plotly.graph_objs as go
import plotly.plotly as py
from datetime import datetime
import igraph
from igraph import Graph, plot


def get_friends(user_id, fields='bdate'):
    domain = "https://api.vk.com/method"
    user_id = str(user_id)

    query_params = {
        'domain': domain,
        'access_token': access_token,
        'user_id': user_id,
        'fields': fields
    }

    query = "{domain}/friends.get?access_token={access_token}&user_id={user_id}&fields={fields}&v=5.53".format(**query_params)
    response = requests.get(query)
    id = []
    if 'error' in response.json():
        return 'error'
    for i in range(response.json()['response']['count']):
        id.append(response.json()['response']['items'][i])
    return id


def age_predict(user_id):
    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    fr = get_friends(user_id)
    for i in range(len(fr)):
        fr[i] = 2017 - fr[i]['bdate'][-4:]
    return sum(fr) / len(fr)


def get(url, params={}, timeout=5, max_retries=5, backoff_factor=0.3):
    for retry in range(max_retries):
        try:
            response = requests.get(url, params=params, timeout=timeout)
            return response
        except requests.exceptions.RequestException:
            if retry == max_retries - 1:
                raise
            backoff_value = backoff_factor * (2 ** retry)
            time.sleep(backoff_value)


def messages_get_history(user_id, offset=0, count=200):
    domain = "https://api.vk.com/method"

    assert isinstance(user_id, int), "user_id must be positive integer"
    assert user_id > 0, "user_id must be positive integer"
    assert isinstance(offset, int), "offset must be positive integer"
    assert offset >= 0, "user_id must be positive integer"
    assert count >= 0, "user_id must be positive integer"

    query_params = {
            'domain': domain,
            'access_token': access_token,
            'user_id': str(user_id),
            'offset': offset,
            'count': min(200, count)
    }

    query = "{domain}/messages.getHistory?access_token={access_token}&user_id={user_id}&count={count}&v=5.53".format(**query_params)
    response = requests.get(query)
    return response.json()


def count_dates_from_messages(messages):
    date = []
    stats = []
    dates = []
    for i in range(len(messages['response']['items'])):
        date.append(datetime.fromtimestamp(messages['response']['items'][i]['date']).strftime("%Y-%m-%d"))
    k = 0
    stats.append(1)
    dates.append(date[0])
    for i in range(1, len(date)):
        if date[i] != date[i - 1]:
            k += 1
            stats.append(0)
            dates.append(date[i])
        stats[k] += 1
    return dates, stats


def plotly_messages_freq(freq_list):
    data = [go.Scatter(x=freq_list[0], y=freq_list[1])]
    plotly.plotly.plot(data)


def get_network(user_id=vkid, as_edgelist=True):
    users_ids = get_friends(user_id, 'id')
    edges = []
    matrix = [[0] * len(users_ids) for i in range(len(users_ids))]
    for i in range(len(users_ids)):
        response = get_friends(users_ids[i]['id'], 'id')
        if response == 'error':
            continue
        for j in range(i + 1, len(users_ids)):
            if users_ids[j] in response:
                if as_edgelist:
                    edges.append((i, j))
                else:
                    matrix[i][j] = 1
    if as_edgelist:
        return edges
    else:
        return matrix


def plot_graph(user_id=vkid):
    surnames = get_friends(user_id, 'last_name')
    vertices = [surnames[i]['last_name'] for i in range(len(surnames))]
    edges = get_network(user_id)
    g = Graph(vertex_attrs={"shape": "circle",
                            "label": vertices,
                            "size": 10},
              edges=edges, directed=False)

    N = len(vertices)
    visual_style = {
        "vertex_size": 200,
        "bbox": (2000, 2000),
        "margin": 100,
        "vertex_label_dist": 1.6,
        "edge_color": "black",
        "autocurve": True,
        "layout": g.layout_fruchterman_reingold(
            maxiter=100000,
            area=N ** 2,
            repulserad=N ** 2)
    }

    clusters = g.community_multilevel()
    pal = igraph.drawing.colors.ClusterColoringPalette(len(clusters))
    g.vs['color'] = pal.get_many(clusters.membership)
    plot(g, **visual_style)
