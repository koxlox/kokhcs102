<?php

include('connection.php');
$connect = new mysqli($host, $user, $pswd, $database);

$connect->query("
CREATE TABLE worker(
	name_worker VARCHAR(40) NOT NULL PRIMARY KEY,
	passport VARCHAR(40) NOT NULL,
	zarplata INT NOT NULL);");

$connect->query("
CREATE TABLE cekh(
    id_cekh INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    chicken_number INT NOT NULL);");

$connect->query("
CREATE TABLE cell(
    id_cell INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    id_row INT NOT NULL,
    id_cekh INT NOT NULL,
	FOREIGN KEY (id_cekh) REFERENCES cekh (id_cekh));");

$connect->query("
CREATE TABLE obsluj(
	name_worker VARCHAR(40) NOT NULL,
	id_cell INT NOT NULL,
	FOREIGN KEY (name_worker) REFERENCES worker (name_worker),
	FOREIGN KEY (id_cell) REFERENCES cell (id_cell));");

$connect->query("
CREATE TABLE diet(
    id_diet INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    consist VARCHAR(40) NOT NULL);");

$connect->query("
CREATE TABLE kind(
    kind_name VARCHAR(40) NOT NULL PRIMARY KEY,
    egg_month INT NOT NULL,
    id_diet INT NOT NULL,
	FOREIGN KEY (id_diet) REFERENCES diet (id_diet));");

$connect->query("
CREATE TABLE chicken(
	id_chicken INT NOT NULL PRIMARY KEY AUTO_INCREMENT,
    eggs INT NOT NULL,
    weight INT NOT NULL,
   	age INT NOT NULL,
	id_cell INT NOT NULL,
	kind_name VARCHAR(40) NOT NULL,
	FOREIGN KEY (id_cell) REFERENCES cell (id_cell),
	FOREIGN KEY (kind_name) REFERENCES kind(kind_name));");

$connect->query("
CREATE TABLE mesto(
	id_chicken INT(40) NOT NULL,
	id_cell INT NOT NULL,
	spec VARCHAR(40) NOT NULL,
	FOREIGN KEY (id_chicken) REFERENCES chicken (id_chicken),
	FOREIGN KEY (id_cell) REFERENCES cell (id_cell));");

$connect->query('INSERT INTO worker  VALUES ("Arlakov", 2323, 14000), ("Pakhmurin", 23423, 13000), ("Nosochenko", 12132, 12000);');

$connect->query('INSERT INTO cekh VALUES (1, 12), (2, 14), (3, 8);');

$connect->query('INSERT INTO cell VALUES(1, 4, 	1), (2, 8, 2), (3, 4, 1);');

$connect->query('INSERT INTO obsluj VALUES("Arlakov", 2), ("Pakhmurin", 1), ("Nosochenko", 3);');

$connect->query('INSERT INTO diet VALUES(1, "vse"), (2, "nichego"), (3, "chto-to");');

$connect->query('INSERT INTO kind VALUES ("Petukh", 0, 1), ("Golub", 2, 2), ("Jolud", 0, 3);');

$connect->query('INSERT INTO chicken VALUES(1, 1, 2, 3, 1, "Golub"), (2, 2, 2, 2, 2, "Golub"), (3, 2, 1, 4, 3, "Petukh");');

$connect->query('INSERT INTO mesto VALUES(1, 1, "special"), (2, 2, "special"), (3, 3, "special");');

?>