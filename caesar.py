def encrypt_caesar(plaintext, shift):
    alph = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
    alph += 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'
    ciphertext = ''
    for e in plaintext:
        if e in alph:
            ciphertext += alph[alph.find(e) + shift % 26]
        else:
            ciphertext += e
    return ciphertext


def decrypt_caesar(ciphertext, shift):
    alph = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
    alph += 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'
    plaintext = ''
    for e in ciphertext:
        if e in alph:
            plaintext += alph[alph.rfind(e) - shift % 26]
        else:
            plaintext += e
    return plaintext
