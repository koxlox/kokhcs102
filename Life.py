import pygame
import random
from pygame.locals import *


class GameOfLife:
    def __init__(self, width=640, height=480, cell_size=10, speed=10):
        self.width = width
        self.height = height
        self.cell_size = cell_size
        self.screen_size = width, height
        self.screen = pygame.display.set_mode(self.screen_size)
        self.cell_width = self.width // self.cell_size
        self.cell_height = self.height // self.cell_size
        self.speed = speed

    def draw_grid(self):
        for x in range(0, self.height, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'), (x, 0), (x, self.width))
        for y in range(0, self.width, self.cell_size):
            pygame.draw.line(self.screen, pygame.Color('black'), (0, y), (self.height, y))

    def run(self):
        pygame.init()
        clock = pygame.time.Clock()
        pygame.display.set_caption('Game of Life')
        self.screen.fill(pygame.Color('white'))
        self.draw_grid()
        A = self.cell_list()
        self.draw_cell_list(A)
        pygame.display.flip()
        clock.tick(self.speed)
        running = True
        while running:
            for event in pygame.event.get():
                if event.type == QUIT:
                    running = False
            self.draw_grid()
            A = self.update_cell_list(A)
            self.draw_cell_list(A)
            pygame.display.flip()
            clock.tick(self.speed)
        pygame.quit()

    def cell_list(self, randomize=True):
        if randomize == True:
            return [[random.randint(0, 1) for j in range(self.height // self.cell_size)] for i in range(self.width // self.cell_size)]
        else:
            return [[0 for j in range(self.height // self.cell_size)] for i in range(self.width // self.cell_size)]

    def draw_cell_list(self, rects):
        screen = pygame.display.set_mode((self.width, self.height))
        for i in range(self.width // self.cell_size):
            for j in range(self.height // self.cell_size):
                if rects[i][j] == 0:
                    pygame.draw.rect(screen, pygame.Color('white'), (i * self.cell_size, j * self.cell_size, self.cell_size, self.cell_size))
                else:
                    pygame.draw.rect(screen, pygame.Color('green'), (i * self.cell_size, j * self.cell_size, self.cell_size, self.cell_size))

    def get_neighbours(self, A, cell):
        x, y = cell[0], cell[1]
        k = 0
        for i in range(y - 1, y + 2):
            for j in range(x - 1, x + 2):
                if i >= 0 and i < len(A) and j >= 0 and j < len(A[i]) and (i != y or j != x):
                    k += A[i][j]
        return k

    def update_cell_list(self, cell_list):
        A = cell_list
        for i in range(len(A)):
            for j in range(len(A[i])):
                if cell_list[i][j] == 1:
                    if not(2 <= self.get_neighbours(cell_list, (i, j)) <= 3):
                        A[i][j] = 0
                elif self.get_neighbours(cell_list, (i, j)) == 3:
                        A[i][j] = 1
        return A

if __name__ == '__main__':
    game = GameOfLife(480, 480, 4)
    game.run()
